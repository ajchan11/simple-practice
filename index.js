const express = require('express')
const app = express()
const port = 3000

const AppointmentsController = require('./controllers/Appointments')

// router here

app.get('/appointments', AppointmentsController.list)

app.listen(port, () => {
  console.log(`Running on Port: ${port}`)
})