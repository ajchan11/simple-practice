customers

id: serial
business_name: varchar
username: varchar

clients
id: serial
customer_id: FK customers.id
name: varchar
notes: varchar

appointments
id: serial
client_id: FK clients.id
start_time: datetime without timezeone
end_time: datetime without timezone
<!-- duration: integer -->


GET /appointments

return all appointment of all clients and the customer they belong to

[{
  id: 1,
  client: {
    id: 2,
    name: 'John',
  },
  customer: {
    id: 3,
    username: 'user1'
  },
  start_time: "12-12-12"
  end_time: "12-12-12"
}]

<!-- {
  appointments: []
  clients: [],
  customers: []
} -->