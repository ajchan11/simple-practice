const AppointmentsService = require('../services/Appointments')

const list = (req, res, next) => {
  const params = req.query
  return AppointmentsService.listAppointments(queryParams)
    .then((results) => {
       return res.status(200).json({ results })
    })
}

module.exports = {
  list
}