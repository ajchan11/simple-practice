const Pool = require('pg').Pool

const pool = new Pool({
  user: 'me',
  host: 'localhost',
  database: 'testing',
  password: 'password',
  port: 5432,
})
 
 const listAppointments = (params) => {
  const query = `
    SELECT 
      appointments.id,
      appointments.start_time,
      appointments.end_time,
      row_to_json(clients.*) as clients,
      row_to_json(customers.*) as customers
    FROM 
      appointments
    LEFT JOIN 
      clients
    ON
      clients.id = appointments.client_id
    LEFT JOIN
      customers
    ON
      customers.id = clients.customer_id
    GROUP BY 
      appointments.id,
      appointments.start_time,
      appointments.end_time,
      clients.*,
      customers.*
  `

   return pool.query(query)
    .then((res) => {
      if (res.rowCount) {
        return res.rows
      } else {
        return []
      }
    })
 }

 module.exports = {
   listAppointments
 }