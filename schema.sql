--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: appointments; Type: TABLE; Schema: public; Owner: me
--

CREATE TABLE public.appointments (
    id integer NOT NULL,
    client_id integer,
    start_time timestamp without time zone,
    end_time timestamp without time zone
);


ALTER TABLE public.appointments OWNER TO me;

--
-- Name: appointments_id_seq; Type: SEQUENCE; Schema: public; Owner: me
--

CREATE SEQUENCE public.appointments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.appointments_id_seq OWNER TO me;

--
-- Name: appointments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: me
--

ALTER SEQUENCE public.appointments_id_seq OWNED BY public.appointments.id;


--
-- Name: clients; Type: TABLE; Schema: public; Owner: me
--

CREATE TABLE public.clients (
    id integer NOT NULL,
    customer_id integer NOT NULL,
    name character varying,
    notes character varying
);


ALTER TABLE public.clients OWNER TO me;

--
-- Name: clients_id_seq; Type: SEQUENCE; Schema: public; Owner: me
--

CREATE SEQUENCE public.clients_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clients_id_seq OWNER TO me;

--
-- Name: clients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: me
--

ALTER SEQUENCE public.clients_id_seq OWNED BY public.clients.id;


--
-- Name: customers; Type: TABLE; Schema: public; Owner: me
--

CREATE TABLE public.customers (
    id integer NOT NULL,
    business_name character varying,
    username character varying NOT NULL
);


ALTER TABLE public.customers OWNER TO me;

--
-- Name: customers_id_seq; Type: SEQUENCE; Schema: public; Owner: me
--

CREATE SEQUENCE public.customers_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.customers_id_seq OWNER TO me;

--
-- Name: customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: me
--

ALTER SEQUENCE public.customers_id_seq OWNED BY public.customers.id;


--
-- Name: appointments id; Type: DEFAULT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.appointments ALTER COLUMN id SET DEFAULT nextval('public.appointments_id_seq'::regclass);


--
-- Name: clients id; Type: DEFAULT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.clients ALTER COLUMN id SET DEFAULT nextval('public.clients_id_seq'::regclass);


--
-- Name: customers id; Type: DEFAULT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.customers ALTER COLUMN id SET DEFAULT nextval('public.customers_id_seq'::regclass);


--
-- Name: appointments appointments_pkey; Type: CONSTRAINT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.appointments
    ADD CONSTRAINT appointments_pkey PRIMARY KEY (id);


--
-- Name: clients clients_pkey; Type: CONSTRAINT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT clients_pkey PRIMARY KEY (id);


--
-- Name: customers customers_pkey; Type: CONSTRAINT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.customers
    ADD CONSTRAINT customers_pkey PRIMARY KEY (id);


--
-- Name: appointments client_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.appointments
    ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES public.clients(id) NOT VALID;


--
-- Name: clients customer_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: me
--

ALTER TABLE ONLY public.clients
    ADD CONSTRAINT customer_id_fk FOREIGN KEY (customer_id) REFERENCES public.customers(id) NOT VALID;


--
-- PostgreSQL database dump complete
--

